/*tabs+popup+script*/
define([
    'jquery',
    'tabs',
    'Magento_Ui/js/modal/modal',
    'mage/translate'

], function ($, tabs, modal, $t) {
    'use strict';

    $.widget('magentis.PopUpSizeChart', {
        options: {
            option: {
                openedState: "active",
                active: 0, "disabled": [2],
                disabledState: "disabled",
                animate: {
                    "duration": 0
                },
            },

            content: '.popupcontent',
            config:{
                types: 'popup',
                responsive: true,
                title: $t('Traditional Ladies Aran Sweater - Size Chart'),
                buttons: [{
                    text: $t('Close'),
                    class: "action"
                }]
            },
        },

        _create: function () {

            $('.popupcontent').tabs(this.options.option);

            let self = this;
            $('.size_chart').on('click', this.element, function () {
                self.showPopUpBox();
            });
        },

        showPopUpBox: function () {
            let content = $(this.options.content);
            modal(this.options.config, content);
            content.modal('openModal');
        }
    });

    return $.magentis.PopUpSizeChart;
});
