/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'SizeChartjs': 'Magento_Catalog/js/size_chart',
    }
};
