/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    paths: {
        'testjs': 'Magento_Theme/js/test',
        'slickjs': 'Magento_Theme/js/slick',
        'slick': 'Magento_Theme/js/external/slick.min',
    }
};
