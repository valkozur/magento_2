/*slick slider*/
define([
    'jquery',
    'slick',

], function ($) {
    'use strict';

    $.widget('magentis.slider', {
        options: {
        },

        _create: function () {
            this.createSlider();
        },

        createSlider: function () {
            $(this.element).slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        }
    });

    return $.magentis.slider;
});
