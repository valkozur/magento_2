

/*slick*/
/*define([
    'jquery',
    'slick',

], function ($) {
    'use strict';

    $.widget('magentis.test', {
        options: {
            wrapper_class: null,
            event: 'click'
        },

        _create: function () {
            this.bind();
            this.createSlider();
        },
        bind: function () {
            $('body').on(this.options.event, function () {

            });

            $(this.options.wrapper_class).on('click', function () {
            })
        },
        createSlider: function () {
            $(this.element).slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });
        }
    });

    return $.magentis.test;
});*/




/*
/!*alert(123);*!/
define([
    'jquery',
    'slick',

], function ($) {
    'use strict';

    $.widget('magentis.test', {
        options: {
            wrapper_class: null,
            event: 'click'
        },

        _create: function () {
            this.bind();
            this.createSlider();
        },
        bind: function () {
            $('body').on(this.options.event, function () {
                alert(12345);
            });

            $(this.options.wrapper_class).on('click', function () {
            })
        },
        createSlider: function () {
            $(this.element).slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 4,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        }
    });

    return $.magentis.test;
});


---------*/
